-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 20, 2017 at 08:57 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecoomerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `brand_id` int(10) NOT NULL,
  `brand_title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`brand_id`, `brand_title`) VALUES
(1, 'HP'),
(2, 'Dell'),
(3, 'Sony'),
(4, 'Nokia'),
(5, 'Samsung'),
(6, 'Motorolla');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `p_id` int(10) NOT NULL,
  `ip_add` int(10) NOT NULL,
  `qty` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`p_id`, `ip_add`, `qty`) VALUES
(1, 0, 0),
(2, 0, 0),
(3, 1270, 0),
(5, 1270, 0),
(6, 1270, 0),
(7, 0, 0),
(8, 1270, 0),
(9, 1270, 0),
(10, 1270, 0),
(11, 1270, 0);

-- --------------------------------------------------------

--
-- Table structure for table `catagories`
--

CREATE TABLE `catagories` (
  `cat_id` int(10) NOT NULL,
  `cat_title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `catagories`
--

INSERT INTO `catagories` (`cat_id`, `cat_title`) VALUES
(1, 'Laptop'),
(2, 'Computer'),
(3, 'Mobiles'),
(4, 'Camera'),
(5, 'Tablets'),
(6, 'Tvs');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `customer_id` int(10) NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `customer_email` varchar(100) NOT NULL,
  `customer_pass` varchar(100) NOT NULL,
  `customer_country` text NOT NULL,
  `customer_city` text NOT NULL,
  `customer_contact` int(100) NOT NULL,
  `customer_address` text NOT NULL,
  `customer_image` text NOT NULL,
  `customer_ip` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customer_id`, `customer_name`, `customer_email`, `customer_pass`, `customer_country`, `customer_city`, `customer_contact`, `customer_address`, `customer_image`, `customer_ip`) VALUES
(16, 'Ahsanul Hoq', 'ahsanlohagara00@gmail.com', '123', 'Afganistan', 'Chittagong', 1835667767, 'ctg,lohagara', 'images (4).jpg', 0),
(17, 'Anamul Hoq', 'anam@gmail.com', '123', 'India', 'Chittagong', 1835667767, 'ctg,lohagara', '', 0),
(18, 'ahsan', 'ahsanmail@gmail.com', '24234', 'Afganistan', 'ctg', 583049583, 'lohagara,ctg', '', 0),
(19, 'salim', 'ahsanmail@gmail.com', '121324', 'Afganistan', 'kabul', 342423421, 'kabul,afg', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer_orders`
--

CREATE TABLE `customer_orders` (
  `order_id` int(10) NOT NULL,
  `customer_id` int(10) NOT NULL,
  `due_amount` int(100) NOT NULL,
  `invoice_no` int(100) NOT NULL,
  `total_products` int(100) NOT NULL,
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_orders`
--

INSERT INTO `customer_orders` (`order_id`, `customer_id`, `due_amount`, `invoice_no`, `total_products`, `order_date`, `order_status`) VALUES
(3, 16, 0, 2111675015, 0, '0000-00-00 00:00:00', 'pending'),
(4, 16, 0, 308863149, 0, '0000-00-00 00:00:00', 'pending'),
(5, 16, 399, 163984779, 1, '0000-00-00 00:00:00', 'pending'),
(6, 16, 0, 1196987088, 0, '0000-00-00 00:00:00', 'pending'),
(7, 16, 1132, 844000837, 1, '0000-00-00 00:00:00', 'pending'),
(8, 16, 1599, 1354364081, 1, '0000-00-00 00:00:00', 'pending'),
(9, 16, 3332, 257956914, 2, '0000-00-00 00:00:00', 'pending'),
(10, 16, 2199, 1830530199, 1, '0000-00-00 00:00:00', 'pending'),
(11, 16, 999, 560676768, 1, '0000-00-00 00:00:00', 'pending'),
(12, 16, 1599, 1788884892, 1, '0000-00-00 00:00:00', 'pending'),
(13, 16, 0, 1893416994, 0, '0000-00-00 00:00:00', 'pending'),
(14, 16, 2943, 1663997177, 4, '0000-00-00 00:00:00', 'pending'),
(15, 16, 299, 1689936858, 1, '0000-00-00 00:00:00', 'pending'),
(16, 0, 0, 694256682, 0, '0000-00-00 00:00:00', 'pending'),
(17, 16, 2642, 1799466412, 3, '0000-00-00 00:00:00', 'pending'),
(18, 16, 1111, 1022903703, 1, '0000-00-00 00:00:00', 'pending'),
(19, 16, 1111, 483280210, 1, '0000-00-00 00:00:00', 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `pending_order`
--

CREATE TABLE `pending_order` (
  `customer_id` int(10) NOT NULL,
  `invoice_no` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  `qty` int(10) NOT NULL,
  `order_status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pending_order`
--

INSERT INTO `pending_order` (`customer_id`, `invoice_no`, `product_id`, `qty`, `order_status`) VALUES
(0, 694256682, 0, 1, 'pending'),
(16, 2111675015, 0, 1, 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(10) NOT NULL,
  `cat_id` int(10) NOT NULL,
  `brand_id` int(10) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `product_title` text NOT NULL,
  `product_img1` varchar(244) NOT NULL,
  `product_img2` varchar(244) NOT NULL,
  `product_img3` varchar(244) NOT NULL,
  `product_price` int(100) NOT NULL,
  `product_desc` text NOT NULL,
  `product_keywords` text NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `cat_id`, `brand_id`, `date`, `product_title`, `product_img1`, `product_img2`, `product_img3`, `product_price`, `product_desc`, `product_keywords`, `status`) VALUES
(1, 1, 1, '2017-04-17 12:27:47', 'Beautiful laptop', 'download (2).jpg.jpg', 'images (1).jpg.jpg', 'download (2).jpg.jpg', 1111, 'this is nice laptop', 'HP,laptops', 'on'),
(2, 3, 1, '2017-04-17 12:21:29', 'New Mobile', 'mobile300.png', 'download.jpg', 'images.jpg', 534, 'This is nice mobile ', 'mobile,samsung,new', 'on'),
(3, 1, 2, '2017-04-17 12:21:47', 'Dell Laptop', 'download (1).jpg.jpg', 'images (1).jpg.jpg', 'download (2).jpg.jpg', 1300, 'This is new Dell laptop', 'dell,laptop,new', 'on'),
(4, 1, 2, '2017-04-17 12:22:05', 'New Dell laptop', 'images (1).jpg.jpg', 'download (1).jpg.jpg', 'download (2).jpg.jpg', 1132, 'This is new Dell laptop for sell', 'new,dell laptop,computer', 'on'),
(5, 3, 5, '2017-04-17 12:22:20', 'Samsung Mobile', 'download.jpg', 'images.jpg', 'mobile300.png', 999, 'This is samsung android mobile', 'samsung,mobile', 'on'),
(6, 4, 5, '2017-04-17 12:22:42', 'New Camera', 'images (16).jpg', 'images (17).jpg', 'images (18).jpg', 599, 'this is nice camera', 'new,camera,samsung', 'on'),
(7, 6, 5, '2017-04-17 12:23:04', 'New TV', 'download (14).jpg', 'download (13).jpg', 'download (15).jpg', 399, 'this is android tv', 'tv,samsung,sony', 'on'),
(8, 5, 1, '2017-04-17 12:23:29', 'New Tablet', 'images (20).jpg', 'images (21).jpg', 'images (22).jpg', 1599, 'this is new tablet device', 'tablets,samsung tab,tab', 'on'),
(9, 2, 2, '2017-04-17 12:23:59', 'New Computer', 'download (4).jpg', 'download (3).jpg', 'download (5).jpg', 2199, 'this is new computer', 'computer,dell computer,samsung destop,destop', 'on'),
(10, 4, 3, '2017-04-17 23:50:13', 'Sony Camera', 'images (19).jpg', 'images (17).jpg', 'images (16).jpg', 299, 'this is nice camera', 'sony', 'on'),
(11, 4, 3, '2017-04-17 23:58:21', 'Sony Camera', 'images (17).jpg', 'images (18).jpg', 'images (16).jpg', 299, 'this is nice camera', 'sony,camera,cameras,sony camera,SONY', 'on');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `catagories`
--
ALTER TABLE `catagories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `customer_orders`
--
ALTER TABLE `customer_orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `pending_order`
--
ALTER TABLE `pending_order`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `brand_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `p_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `catagories`
--
ALTER TABLE `catagories`
  MODIFY `cat_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `customer_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `customer_orders`
--
ALTER TABLE `customer_orders`
  MODIFY `order_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
